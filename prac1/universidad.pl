:- module(_,[],[]).

% :- doc(author,"Laura Garcia u120335").
% :- doc(author,"Eugenio Gonzalo q080099").
% :- doc(author,"Santiago Cervantes r090124").

%%%%%%%%%%%
%% FACTS %%
%%%%%%%%%%%

tieneDoctorado('Ricardo Imbert').
tieneDoctorado('Manuel Hermenegildo').
tieneDoctorado('Manuel Carro').
tieneDoctorado('Pablo Nogueira').
tieneDoctorado('Damiano Z.').
tieneDoctorado('Miguel Garcia').
tieneDoctorado('Hans Solo').
tieneDoctorado('Pedro Angustias').
tieneDoctorado('Pedro Doolittle').

esIngeniero('Guillermo').
esIngeniero('Pedro').
esIngeniero('Oscar').
esIngeniero('Juan').
esIngeniero('Pau').
esIngeniero('Alvaro').

hablaIdioma('Ricardo Imbert',chino).
hablaIdioma('Manuel Hermenegildo',ingles).
hablaIdioma('Manuel Carro',chino).
hablaIdioma('Damiano Z.',ingles).
hablaIdioma('Pablo Nogueira',ingles).
hablaIdioma('Miguel Garcia',ingles).
hablaIdioma('Hans Solo',wookie). % No unifica
hablaIdioma('Pedro Angustias',esperanto). % No unifica
hablaIdioma('Pedro Doolittle',ingles).
hablaIdioma('Pau',ingles).
hablaIdioma('Alvaro',chino).

consiguePlaza('Manuel Hermenegildo').
consiguePlaza('Manuel Carro').
consiguePlaza('Ricardo Imbert').
consiguePlaza('Pablo Nogueira').
consiguePlaza('Damiano Z.').
consiguePlaza('Miguel Garcia').
consiguePlaza('Guillermo').
consiguePlaza('Pedro').
consiguePlaza('Oscar').
consiguePlaza('Juan').

espaniol('Ricardo Imbert').
espaniol('Manuel Hermenegildo').
espaniol('Manuel Carro').
espaniol('Pablo Nogueira').
espaniol('Miguel Garcia').

perteneceDepartamento('Ricardo Imbert','DLSIIS').
perteneceDepartamento('Manuel Hermenegildo','DIA').
perteneceDepartamento('Manuel Carro','DIA').
perteneceDepartamento('Pablo Nogueira','DLSIIS').
perteneceDepartamento('Miguel Garcia','DIA').
perteneceDepartamento('Damiano Z.','DIA').

edad('Ricardo Imbert',s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0)))))))))))))))))))))))))))))))))))))))).
edad('Manuel Hermenegildo',s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))).
edad('Manuel Carro',s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0)))))))))))))))))))))))))))))))))))))))))))))).
edad('Pablo Nogueira',s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))))))))))))))))))))))))))))))).
edad('Damiano Z.',s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0)))))))))))))))))))))))))))))))))))))).
edad('Damiano Z.',s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))))))))))))))))))))))))))))))).

dirigeInvestigacion('Manuel Hermenegildo','Guillermo').
dirigeInvestigacion('Manuel Hermenegildo','Manuel Carro').
dirigeInvestigacion('Manuel Hermenegildo','Miguel Garcia').
dirigeInvestigacion('Manuel Carro','Pedro').
dirigeInvestigacion('Manuel Carro','Pablo Nogueira').
dirigeInvestigacion('Pablo Nogueira','Oscar').
dirigeInvestigacion('Pablo Nogueira','Damiano Z.').
dirigeInvestigacion('Damiano Z.','Juan').
dirigeInvestigacion('Damiano Z.','Ricardo Imbert').

rector(robles).

cursaAsignatura(yago,[prolog,sos,distribuidos]).
cursaAsignatura(eugenio,[prolog,sos,distribuidos]).
cursaAsignatura(laura,[prolog,sos,distribuidos]).
cursaAsignatura(pedro,[ssoo]).
cursaAsignatura(eusebio,[logica,ssoo]).
cursaAsignatura(sandra,[bioinspirada]).
cursaAsignatura(jorge,[segti]).
cursaAsignatura(luis,[discretas,segti]).
cursaAsignatura(peter,[]).

asignaturaDepartamento(prolog,'DIA').
asignaturaDepartamento(distribuidos,'DATSI').
asignaturaDepartamento(sos,'DLSIIS').
asignaturaDepartamento(ssoo,'DATSI').
asignaturaDepartamento(logica,'DIA').
asignaturaDepartamento(bioinspirada,'DIA').
asignaturaDepartamento(segti,'DLSIIS').
asignaturaDepartamento(discretas,'DMA').

parteDeGrupo(yago,'prolog_1').
parteDeGrupo(laura,'prolog_1').
parteDeGrupo(eugenio,'prolog_1').
parteDeGrupo(pedro,'ssoo_32').
parteDeGrupo(sandra,'bioins_13').
parteDeGrupo(eusebio,'ssoo_29').

asignaturaPractica(distribuidos).



%%%%%%%%%%%%%%%%%%%%
%% CUERPO DOCENTE %%
%%%%%%%%%%%%%%%%%%%%

%% personalDocente(x) iff (profesor(x) v auxiliarDocente(x))  

%% pred personalDocente(+Persona) :: atm
:- export(personalDocente/1).
personalDocente(X) :-
        profesor(X).
personalDocente(X) :-
        auxiliarDocente(X).

%% Basicamente lo que viene a decir este predicado es que alguien forma parte
%% del personal docente de la universidad si es un profesor o un auxiliar de
%% profesor.


%% profesor(x) iff (tieneDoctorado(x) ^ hablaIdioma(x,y) ^
%%               (y = ingles v y == chino) ^ consiguePlaza(x)) 

%% pred profesor(+Persona) :: atm
:- export(profesor/1).
profesor(X) :-
        tieneDoctorado(X),
        hablaIdioma(X,ingles),   %% <--
        consiguePlaza(X).        %%   |
profesor(X) :-                   %%   |-- En el caso de que hable solo uno, al fallar
        tieneDoctorado(X),       %%   |   entraria en el segundo case del predicado
        hablaIdioma(X,chino),    %% <--
        consiguePlaza(X).

%% Para que una persona X sea profesor de la universidad tiene que tener un
%% doctorado, hablar ingles o chino y haber conseguido una plaza.


%% auxiliarDocente(x) iff esIngeniero(x) ^ consiguePlaza(x) 

%% pred auxiliarDocente(+Persona) :: atm
:- export(auxiliarDocente/1).
auxiliarDocente(X) :-
        esIngeniero(X),
        consiguePlaza(X).

%% Por contra, X es auxiliarDocente si es ingeniero y ha consegido una plaza. 


%% asignaAsignaturaRector(x,y,z) iff
%%   (profesor(x) ^ recctor(z) ^ perteneceDepartamento(x,Dep) ^ asignaturaDepartamento(Y,Dep)) v (auxiliarDocente(x) ^ recctor(z) ^ asignaturaPractica(y))

%% pred asignaAsignaturaRector(+Asignatura,+Profesor,+Rector) :: atm * atm * atm
:- export(asignaAsignaturaRector/3).
asignaAsignaturaRector(X,Y,Z) :-
        profesor(X),
        rector(Z),
        perteneceDepartamento(X,Department),
        asignaturaDepartamento(Y,Department).
asignaAsignaturaRector(X,Y,Z) :-
        auxiliarDocente(X),
        rector(Z),
        asignaturaPractica(Y).

%% Para que un profesor X imparta una asignatura Y tiene que ser por peticion del rector
%% Z, y que la asignatura pertenezca al mismo departamento que el profesor. Por otro
%% Un auxiliar docente puede impartir una asignatura por peticion del rector si la
%% asignatura es puramente practica.


%% imprateAsignatura(x,y) iff 
%% (profesor(x) ^ perteneceDepartamento(x,z) ^ asignaturaDepartamento(y,z))%% v (auxiliarAsignatura(x) ^ asignaturaPractica(y))  

%% pred imparteAsignatura(+Persona,+Asignatura) :: atm * atm
:- export(imparteAsignatura/2).
imparteAsignatura(X,Y) :-
        asignaAsignaturaRector(X,Y,_).

%% Una persona X imparte la asignatura Y Si se lo ha asignado el rector.


%% pred mentor(+Mentor,+Telemaco) :: atm * atm
:- export(mentor/2).
mentor(X,Y) :-
        profesor(X),
        auxiliarDocente(Y).

%% Un Profesor X puede ser mentor de un auxiliar Y.


%% designadoTutor(x,y,z) iff (decano(x) ^ personalDocente(y) ^ alumno(z))
%%                       

%% pred designadoTutor(+Tutor,+Tutelado) :: atm * atm
:- export(designadoTutor/3).
designadoTutor(X,Y,Z) :-
        decano(X),
        personalDocente(Y),
        alumno(Z).

%% El decano X designa tutor a Y del alumno Z si Y este forma parte del personal
%% docente


%% tutorProyectoInicio(x) iff designadoTutor(x) 

%% pred tutorProyectoInicio(+Tutor,+Tutelado) :: atm * atm
:- export(tutorProyectoInicio/2).
tutorProyectoInicio(X,Y) :-
        designadoTutor(_,X,Y).

%% X puede ser designado tutor del proyecto inicio si puede ser designado tutor


%% tutorCurso(x,y,z) iff (directorDepartamento(x) ^ profesor(y) ^ alumno(z)) 

%% pred tutorCurso(+DirDep,+Tutor,+Tutelado) :: atm * atm
:- export(tutorCurso/3).
tutorCurso(Z,X,Y) :-
        directorDepartamento(Z),
        profesor(X),
        alumno(Y).

%% El director de departamento Z puede asignar como tutor de curso a el profesor
%% X del alumno Y.


%% tutor(x) iff tutorProyectoInicio(x,y) v tutorCurso(x,y)

%% pred tutor(+Tutor) :: atm
:- export(tutor/2).
tutor(X,Y) :-
        tutorProyectoInicio(X,Y).
tutor(X,Y) :-
        tutorCurso(_,X,Y).

%% X es tutro de algun tipo del alumno Y si es tutor del proyecto inicio del mismo
%% o si es tutor de curso de Y.


%% superiorInmediato(x,y) iff (perteneceDepartamento(x,z) ^
%%              perteneceDepartamento(y,z) ^ supervisor(x,y)) 

%% pred superiorInmediato(+Jefe,+Trabajador) :: atm * atm
:- export(superiorInmediato/2).
superiorInmediato(X,Y) :-
        perteneceDepartamento(X,Z),
        perteneceDepartamento(Y,Z),
        X \= Y,
        supervisor(X,Y).


%% supervisor(x,y) iff
%%      (profesor(x) ^ dirigeInvestigacion(x,y) ^ personalDocente(y))
%%      

%% pred supervisor(+Supervisor,+Supervisado) :: atm * atm
:- export(supervisor/2).
supervisor(X,Y) :-
        profesor(X),
        dirigeInvestigacion(X,Y).

%% X es el supervisor de Y si X es profesor y dirige la investigacion de Y.


%% colega(x,y) iff ((profesor(x) ^ profesor(y)) v
%%           (auxiliarDocente(x) ^ auxiliarDocente(y)) ^
%%             superiorInmediato(x) == superiorInmediato(y)) 

%% pred colega(+Persona1,+Persona2) :: atm * atm
:- export(colega/2).
colega(X,Y) :-
        profesor(X),
        profesor(Y),
        X \= Y,
        superiorInmediato(Z,X),
        superiorInmediato(Z,Y).
colega(X,Y) :-
        auxiliarDocente(X),
        auxiliarDocente(Y),
        X \= Y,
        superiorInmediato(Z,X),
        superiorInmediato(Z,Y).

%% Dos personas son colegas si son los dos profesores o auxiliares y tienen el
%% mismo superior inmediato. Notese el predicado de "No unificacion" para que
%% no se de el caso de que una persona es amiga de si misma.


%% decano(x) iff  nombradoDecanoRector(x,_) 

%% pred decano(+Decano) :: atm 
:- export(decano/1).
decano(X) :-
        nombradoDecanoRector(X,_).

%% X sera decano si puede ser nombrado como tal por el rector del momento.


%% nombradoDecanoRector(x,y) iff (decano(y) ^ edad(x) > 30 ^
%%            profesor(x) ^ hablaIdioma(x,espaniol)) 

%% pred nombradoDecanoRector(+Profesor,+Rector) :: atm * atm
:- export(nombradoDecanoRector/2).
nombradoDecanoRector(X,Y) :-
        rector(Y),
        edad(X,Age),
        mayor_igual_30(Age),
        profesor(X),
        espaniol(X).

%% X podra nombrar decano a Y si X es rector, la edad de X es mayor o igual que
%% 30, es profesor y de origen espaniol.


%% designado(x,y) iff (profesor(x) ^ decano(y) ^ supervisor(x,_)) 

%% pred desigandoPor(+Profesor,+Decano) :: atm * atm 
:- export(designadoPor/2).
designadoPor(X,Y) :-
        profesor(X),
        decano(Y),
        X \= Y,
        supervisor(X,Z),
        auxiliarDocente(Z).

%% El decano Y podra nombrar a X como director de un departamento si supervisa a
%% un auxiliar docente. Igual que antes notese el predicado operador de "No unifoca"
%% Para impedir que el decano se nombre a si mismo como director de departamento.


%% directorDepartamento(X) iff designadoPor(x,y) 

%% pred directorDepartamento(+Profesor) :: atm 
:- export(directorDepartamento/1).
directorDepartamento(X) :-
        designadoPor(X,_).

%% X sera director de departamento si ha sido asignado por el decano del momento.


%% pred superior(+Superior,+Subordinado) :: atm * atm
:- export(superior/2).
superior(X,Y) :-
        superiorInmediato(X,Y).
superior(X,Y) :-
        X \= Z,
        superior(X,Z),
        superiorInmediato(Z,Y).

% :- export(superior/2).
% superior(X,Y) :-
%         superiorInmediato(Z,Y),
%         superior(X,Z).
% superior(X,Y) :-
%         superiorInmediato(X,Y).


%% pred subordinado(+Superior,+Subornidado) :: atom * atm
:- export(subordinado/2).
subordinado(X,Y) :-
        superior(Y,X).

%%%%%%%%%%%%%%
%% ALUMNADO %%
%%%%%%%%%%%%%%

%% alumno(x) iff cursaAsignatura(x,y) 

%% pred alumno(+Alumno) :: atm 
:- export(alumno/1).
alumno(X) :-
        cursaAsignatura(X,[_Y|_]).

%% Una persona X sera alumna si esta matricualda en al menor una asignatura.


%% comapnieros(x,y) iff (cursaAsignatura(x,z) ^ cursaAsignatura(y,z)) 

%% pred companiero(+Alumno1,+Alumno2) :: atm * atm 
:- export(companiero/2).
companiero(X,Y) :-
        cursaAsignatura(X,Asignaturas0),
        cursaAsignatura(Y,Asignaturas1),
        X \= Y,
        compartenAsignatura(Asignaturas0,Asignaturas1).

%% Dos personas seran companieras si son alumnos y comparten la misma
%% Asignatura


%% amigo(x,y) iff companiero(x,y) ^ parteDeGrupo(x,z) ^ parteDeGrupo(y,z)

%% pred amigo(+Alumno1,+Alumno2) :: atm * atm 
:- export(amigo/2).
amigo(X,Y) :-
        companiero(X,Y),
        parteDeGrupo(X,Z),
        parteDeGrupo(Y,Z).

%% A parte, dos personas que sean companieras seran amigas si forman parte
%% del mismo grupo.


%% competidor(x,y) iff companiero(x,y) ^ tutor(z,x) ^ tutor(z,y) ^
%%                grupo(x) /= grupo(y)

%% pred competidor(+Alumno1,+Alumno2) :: atm * atm 
:- export(competidor/2).
competidor(X,Y) :-
        companiero(X,Y),
        parteDeGrupo(X,Z),
        parteDeGrupo(Y,T),
        Z \= T,
        tutor(Prof0,X),
        profesor(Prof0),
        tutor(Prof1,Y),
        profesor(Prof1).

%% A parte de todo esto, dos alumnos X e Y podran ser competidores si solo son
%% alumnos, no forman parte del mismo grupo y tienen un tutor cada uno. IMPORTANTE
%% Este predicado es extremadamente largo en resolucion, dado que cada par de
%% alumnos competidores se repetira tantas veces como combinaciones de tutores
%% haya, dando lugar a un sin fin de posibilidados.



%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PREDICADOS AUXILIARES %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% pred mayor_igual_30(+Age) :: structure 
mayor_igual_30(Age) :-
        mayor_igual_30_(Age,s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))))))))))))))))))))).

%% pred mayor_igual_30_(+Age,+Num) :: structure * structure
mayor_igual_30_(0,0).
mayor_igual_30_(s(_),0).
mayor_igual_30_(s(X),s(Y)) :-
        mayor_igual_30_(X,Y).

%% Este predicado recibe como argumento un numero en terminos de peano y llama a un
%% predicado auxiliar para comprobar si dicho numero es mayo o igual que 30, tambien
%% expresado en terminos de peano.


%% compartenAsignatura(+Asignaturas1,Asignaturas2) :: list * list
:- export(compartenAsignatura/2).
compartenAsignatura([],[]).
compartenAsignatura([X0|_],Asignaturas) :-
        compartenAsignatura_(X0,Asignaturas).
compartenAsignatura([_|Xs0],Asignaturas) :-
        compartenAsignatura(Xs0,Asignaturas).

%% compartenAsignatura_(+Asignatura1,Asignaturas2) :: atm * list
compartenAsignatura_(X,[X|_]).
compartenAsignatura_(X,[Y|Ys]) :-
        X \= Y,
        compartenAsignatura_(X,Ys).

%% este predicado recibe dos listas de tamanio cualquiera y devuelve true si en ellas
%% existe al menos un elemento compartido, en caso contrario devuelve false. Es
%% usado para ver si dos personas comparten asignatura.

