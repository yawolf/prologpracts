:- module(laberinto_llaves,[],[fsyntax]). % fsyntax package, for Functional Syntax!

%% Santiago Cervantes r090124
%% Eugenio Gonzalo q080099
%% Laura Garcia u120335

:- use_module('laberinto').
:- use_module(library(lists),[length/2]). % We only need length predicate

:- ensure_loaded(laberinto).

tail([],[]).
tail([_|Xs],Xs).

head([],[]).
head([X|_],X).

third(L,~head(~tail(~tail(L)))).

open_door(A,PA,HV,NL,NNL,C) :-
        (p(A,C,NL0) ; p(C,A,NL0)),
        (1 < ~length(HV) -> ~third(HV) \= C ; true),
        (
            (member([A,C],PA) ; member([C,A],PA)) ->
            NNL = NL
        ;
            NNL is NL - NL0,
            NNL >= 0
        ).

get_keys(A,HV,NL,NL0) :-
        (
            member(A,HV) ->
            NL0 = NL
        ;
            NL0 is NL + ~e(A)
        ).

camino_(A,A,_,_,_,[A]) :- !.
camino_(A,B,NL,HV,PA,[A|R]) :-
        open_door(A,PA,HV,~get_keys(A,HV,NL),NNL,B), !,
        camino_(B,B,NNL,[A|HV],[[A,B]|PA],R).
camino_(A,B,NL,HV,PA,[A|R]) :-
        open_door(A,PA,HV,~get_keys(A,HV,NL),NNL,C),
        camino_(C,B,NNL,[A|HV],[[A,C]|PA],R).

:- export(camino/3).
camino(A,B,~camino_(A,B,0,[],[])) :- !.
% Thats cuz we only want one Path (del '!' if not!).
