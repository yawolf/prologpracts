:- module(cartesiantree,[],[]).

%%Laura Garc�a Garc�a u120335
%%Eugenio Gonzalo Jim�nez q080099
%%Santiago Cervantes Reus r090124

%% Generates the cartesian tree from a list.
:- export(cartesian_tree/2).
cartesian_tree([],void).
cartesian_tree(List,tree(M,RLeft,RRight)) :-
        minor(List,M),
        split(List,M,Left,Right),
        cartesian_tree(Left,RLeft),
        cartesian_tree(Right,RRight).

%% Compare if a number X is lower than the number Y in peano terms 
peano_minor(0,s(_)).
peano_minor(s(X),s(Y)) :- peano_minor(X,Y).

%% Compare if a number X is bigger or equal than the number Y in peano terms 
peano_beq(0,0).
peano_beq(s(_),0).
peano_beq(s(X),s(Y)) :- peano_beq(X,Y).

%% split a list in the Number element (starts in 1), Peano terms
%% split([1,2,3,4,5,6,7,8,9],4,L,R).
%% L = [1,2,3,4]
%% R = [5,6,7,8,9]
split([X|Xs],X,[],Xs).
split([X|Xs],Elem,[X|Ret],D) :-
        X \= Elem,
        split(Xs,Elem,Ret,D).

%% searches the minor element in the list. Peano terms
%% minor([,5,4,3,2,1,9,7,6,5],M)
%% M = 1
minor([X|Xs],R) :-
        is_the_minor(X,Xs,R).

%% Auxiliar predicate.
is_the_minor(E,[],E).
is_the_minor(E,[X|Xs],R) :-
        peano_minor(X,E),
        is_the_minor(X,Xs,R).
is_the_minor(E,[X|Xs],R) :-
        peano_beq(X,E),
        is_the_minor(E,Xs,R).
