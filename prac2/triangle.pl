:- module(triangle,[],[]).

%%Laura Garc�a Garc�a u120335
%%Eugenio Gonzalo Jim�nez q080099
%%Santiago Cervantes Reus r090124

%% Generates the pascal triangle
:- export(pascal_triangle/2).
pascal_triangle(X,R) :-
        pascal_triangle(0,X,R).

%% Auxiliar predicate
pascal_triangle(X,X,[R]) :-
        genLine(X,R).
pascal_triangle(N,X,[Ret2|R]) :-
        N \= X,
        genLine(N,Ret2),
        pascal_triangle(s(N),X,R).

%% Sums 2 numbers in Peano terms
%% X + Y
sum_peano(0,0,0).
sum_peano(s(X),0,s(X)).
sum_peano(0,s(X),s(X)).
sum_peano(s(X),s(Y),s(s(R2))) :- sum_peano(X,Y,R2).

%% Generate the X line of the pascal triangle
genLine(0,[s(0)]).
genLine(s(X),[s(0)|Ret]) :-
        genLine(X,Line),
        genLine_(Line,Ret).

%% Heart of the pascal triangle generation, it is only
%% the sum 2 by 2 of the last line terms. 
genLine_([s(0)],[s(0)]).
genLine_([X1,X2|Xs],[X3|Ret]) :-
        sum_peano(X1,X2,X3),
        genLine_([X2|Xs],Ret).
